import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { CanvasComponent } from './canvas/canvas.component';
import { VendorListComponent } from './vendor-list/vendor-list.component';
import { GNoteComponent } from './gnote/gnote.component';
import { HttpClientModule } from '@angular/common/http';
import { MainService } from './main.service';

@NgModule({
  declarations: [
    AppComponent,
    CanvasComponent,
    VendorListComponent,
    GNoteComponent
  ],
  imports: [
    BrowserModule,
     FormsModule,
      HttpClientModule
    
  ],
  providers: [MainService],
  bootstrap: [AppComponent]
})
export class AppModule { }
