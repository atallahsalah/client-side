import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GNoteComponent } from './gnote.component';

describe('GNoteComponent', () => {
  let component: GNoteComponent;
  let fixture: ComponentFixture<GNoteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GNoteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GNoteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
